class_name Card
extends Area2D
## General class for cards in the game. 
## [PlayingCard], [StartCard] and [IntimacyCard] inherit from this.
## Supports all card interactions like flipping, enlarging and dragging.

## List of all possible states a [Card] can be in.
enum CARD_STATES {
	RESTING, ## The [Card]'s default state.
	HOVERING, ## The cursor is over the [Card].
	DRAGGING, ## The cursor is dragging the [Card].
}

signal played


## Current state of the [Card], from the [code]CARD_STATES[/code].
var current_state: int = CARD_STATES.RESTING
var _highlight_tween: Tween
var location: GameInfo.CardLocations 

## reference to the associated card front node in the template
@onready var card_front: Control = $Template/Front
## Reference to the associated card back node in the template.
@onready var card_back: Control = $Template/Back
## Reference to the card flip AnimationPlayer node.
@onready var flip_animation: AnimationPlayer = $Template/CardFlip


# Calls dragging and enlarging every frame as aproppriate.
func _process(_delta):
	match current_state:
		CARD_STATES.HOVERING:
			enlarge()
		CARD_STATES.DRAGGING:
			reduce()
			drag()


#func _ready():
	#PlaceCardRule.get_place_card_node(self).entered_timeline.connect(entered_timeline)
	#DrawCardRule.get_draw_card_node(self).entered_hand.connect(entered_hand)
		##get_node("/root/GameState/CoreRules/PlaceCard").connect("entered_timeline", entered_timeline)


## Updates the [PlayingCard]'s global position to follow the cursor.
## Ensures the card renders on top of everything while dragging.
## If pressing stops at any point while dragging, the card is dropped.
func drag() -> void:
	set_global_position(get_viewport().get_mouse_position())
	z_index = RenderingServer.CANVAS_ITEM_Z_MAX
	
	if not Input.is_action_pressed("left_click"):
		set_state("HOVERING")
		z_index = 0


## Enlarges the card and brings it to front while hovering it, to make it easier to read.
## The card is not enlarged if it's facing down or while hovering or dragging another card simultaneously.
func enlarge() -> void:
	
	var all_cards = get_tree().get_nodes_in_group("cards")
	
	if is_facing_up() and all_cards.all(func(card): return card.is_state("RESTING") or card == self):
		_highlight_tween = get_tree().create_tween()
		_highlight_tween.tween_property(self, "scale", Vector2(1.25, 1.25), 0.25)



## Reduces an enlarged card back to the normal size.
## The card is not enlarged if it's facing down or while hovering or dragging another card simultaneously.
func reduce() -> void:
	if not is_state("HOVERING"):
		_highlight_tween = get_tree().create_tween()
		_highlight_tween.tween_property(self, "scale", Vector2(1, 1), 0.25)
		
		
## Flips the card to chosen face.
## If [code]turn_face_up[/code] is [code]true[/code], turns the card face up.
## If no face is chosen, flips to alternate face (front/back).
## Triggers fancy rotating animation.
func flip(turn_face_up: bool = !is_facing_up()) -> void:
	if is_facing_up() != turn_face_up:
		if turn_face_up or !is_facing_up():
			flip_animation.play("card_flip")
		else:
			flip_animation.play_backwards("card_flip")


## Returns [code]true[/code] if card is facing up and [code]false[/code] otherwise.
func is_facing_up() -> bool:
	return card_front.z_index == 0


## Returns true if the [code]current_state[/code] of the [Card] matches the given [code]satate[/code]
func is_state(state: String) -> bool:
	return current_state == CARD_STATES[state]


## Returns the [String] key for the [code]current_state[/code] of the [Card].
func get_state() -> String:
	return CARD_STATES.keys()[current_state]


## Updates the [code]current_state[/code] of the [Card] to a chosen state, from [String] key.
func set_state(new_state: String) -> void:
	current_state = CARD_STATES.keys().find(new_state)


# Sets hovering state when mouse enters the card area while it's resting.
func _on_mouse_entered() -> void:
	if is_state("RESTING"):
		set_state("HOVERING")
		if self.location == GameInfo.CardLocations.HAND:
			$CardHover.play ()


# Sets resting state when mouse exits the card area while it's being hovered.
func _on_mouse_exited() -> void:
	if is_state("HOVERING"):
		set_state("RESTING")
		reduce()
		
		


# Allows manual card flippig in the FreeBoard (for testing).
func _on_input_event(_viewport, event, _shape_idx) -> void:
	if get_parent().get_name() == "FreeBoard":
		if event is InputEventMouseButton and event.double_click:
			flip()


func entered_timeline(card: Card):
	if card == self:
		#print("carta entrou na timeline!")
		location = GameInfo.CardLocations.TIMELINE


func entered_hand(card: Card):
	if card == self:
		#print("carta entrou na mão!")
		location = GameInfo.CardLocations.HAND
	

func entered_drawpile(card: Card):
	if card == self:
		#print("carta entrou na pilha de compra!")
		location = GameInfo.CardLocations.DRAWPILE

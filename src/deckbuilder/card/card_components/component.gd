class_name Component
extends Node
## Superclass for the components a [Card] can have,

func build(parameters: Dictionary):
	return self._build(parameters)
	
	
func _build(_parameters: Dictionary):
	push_error("Virtual Method")
	return null

extends EffectComponent


# amount of cards to be removed
var amount: int


func _build(parameters: Dictionary):
	activation_location = parameters.get("activation_location", null)
	amount = parameters.get("amount", 0)


func _activate():
	var date_cards: Array[Node]
	var player_cards: Array[Node]
	
	# get timeline
	var timeline := GameState.get_game_state_node(self).timeline as Timeline
	var card_owner := get_parent().card_owner as Character
	
	if card_owner is Player:
		date_cards = timeline.date.hand.card_list.get_children()
		player_cards = get_parent().card_owner.hand.card_list.get_children()
		
		date_cards.shuffle()
		
		if date_cards.size() >= amount:
			for i in amount:
				Rulebook.resolve(Effect.remove_card(timeline.date.hand, -1, date_cards[i]), GameState.get_game_state_node(self))
				card_owner.hand.add_card(date_cards[i])
	else:
		player_cards = timeline.player.hand.card_list.get_children()
		date_cards = get_parent().card_owner.hand.card_list.get_children()
		
		player_cards.shuffle()
		
		if player_cards.size() >= amount:
			for i in amount:
				Rulebook.resolve(Effect.remove_card(timeline.player.hand, -1, player_cards[i]), GameState.get_game_state_node(self))
				card_owner.hand.add_card(player_cards[i])
			
		

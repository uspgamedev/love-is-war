class_name EffectComponent
extends Component


var activation_location: GameInfo.CardLocations


func activate():
	return self._activate()


func _activate():
	push_error("Virtual Method")
	return null

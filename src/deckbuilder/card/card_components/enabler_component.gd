class_name EnablerComponent
extends Component


signal effect_enabled


func try_effect():
	return self._try_effect()
	
	
func _try_effect():
	push_error("Virtual Method")
	return null

extends EnablerComponent


var enter_location: GameInfo.CardLocations
var child_components: Array[EffectComponent]


func _build(parameters: Dictionary):
	enter_location = parameters.get("enter_location", null)
	
	match enter_location:
		GameInfo.CardLocations.DRAWPILE:
			pass
		
		GameInfo.CardLocations.HAND:
			DrawCardRule.get_draw_card_node(self).entered_hand.connect(_entered_location)
			
		GameInfo.CardLocations.TIMELINE:
			get_tree().get_first_node_in_group(&"PlaceCardRule").entered_timeline.connect(_entered_location)
			#PlaceCardRule.get_place_card_node(self).entered_timeline.connect(_entered_location)


func _entered_location(card: Card):
	if card == get_parent():
		_try_effect()


func _try_effect():
	emit_signal("effect_enabled")
	#print("tentando efeito!")

	# single use only: disconnects from every effect after being used.
	# Permanent? Maybe	
	var connections = effect_enabled.get_connections()
	for connection in connections:
		if effect_enabled.is_connected(connection["callable"]):
			effect_enabled.disconnect(connection["callable"])

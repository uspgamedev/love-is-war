extends EffectComponent


@export var amount: int = 1

@onready var card_owner = get_parent().card_owner


func _build(parameters: Dictionary):
	activation_location = parameters.get("activation_location", null)
	amount = parameters.get("amount", 0)
	

func _activate():
	if get_parent().location == activation_location:
		print("effect activated! Earning self-esteem...")
		print(card_owner)
		Rulebook.resolve(Effect.change_self_esteem(card_owner, amount), GameState.get_game_state_node(self))
		

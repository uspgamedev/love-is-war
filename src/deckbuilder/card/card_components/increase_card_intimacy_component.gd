extends EffectComponent

@export var amount: int = 1
@export var rounds: int = 1

@onready var card_owner = get_parent().card_owner
var counter = 0

func _build(parameters: Dictionary):
	activation_location = parameters.get("activation_location", null)
	amount = parameters.get("amount", 0)
	rounds = parameters.get("rounds", 0)

func _activate():
	if get_parent().location == activation_location:
		counter += 1
		print("effect activated! Increasing intimacy level...")
		print(card_owner)
		var game_state = GameState.get_game_state_node(self)
		var curr_intimacy = game_state.intimacy_level
  
		if counter == 1:
			var new_intimacy = curr_intimacy
			new_intimacy += amount
			game_state.update_intimacy_level(new_intimacy)
   
		if counter == rounds + 1:
			var new_intimacy = curr_intimacy
			new_intimacy -= amount
			game_state.update_intimacy_level(new_intimacy)

extends EffectComponent


# amount of cards to be removed
var cards_to_remove: int


func _build(parameters: Dictionary):
	activation_location = parameters.get("activation_location", null)
	cards_to_remove = parameters.get("cards_to_remove")


func _activate():
	# get the timeline 
	var timeline := GameState.get_game_state_node(self).timeline
	
	# get the current list of cards in the timeline
	var card_list := timeline.card_list.get_children() as Array[Node]
	
	# get current and start cards
	var current_card := get_parent() as Node
	var start_card := timeline.start_card as Node
	
	# remove those cards from list
	card_list.erase(current_card)
	card_list.erase(start_card)
	
	# shuffle array
	card_list.shuffle()
	
	if card_list.size() >= cards_to_remove:
		for i in cards_to_remove:
			Rulebook.resolve(Effect.remove_card(timeline, -1, card_list[i]), GameState.get_game_state_node(self))

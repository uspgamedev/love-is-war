extends EffectComponent


# list of indexes of the cards to be sticked, relative to the card
# which has this component (e.g: 0 -> this card, -1 -> the card before this one)
@export var sticked_state: PackedScene

var cards_to_stick: Array[int] = []
var sticked_cards_list: Array[Card] = []



# maybe in the future create an EnablerComponent to make this (Conditional effect? Permanent effect?)
# this makes so that for every card that enters the timeline, this component
# checks if it sticks to the card that has it
func _ready():
	PlaceCardRule.get_place_card_node(self).entered_timeline.connect(_try_activate)


func _build(parameters: Dictionary):
	activation_location = parameters.get("activation_location", null)
	for card_idx in parameters.get("cards_to_stick"):
		cards_to_stick.append(card_idx)


func _try_activate(card: Card):
	if card == self:
		_activate()


# sticks as many cards as possible on first activation, and 
# while the number of cards to stick to is not 0 (not cards_to_stick.is_empty()),
# it tries to stick a card when that position in the card_list is filled
func _activate():
	var card_list: Node2D
	var card_idx: int
	# problem: effect happening after a match (the card isn't in the tree anymore)
	# hacky workaround because I'm not smart enough
	if self.is_inside_tree():
		card_list = GameState.get_game_state_node(get_parent()).timeline.card_list
		card_idx = card_list.get_children().find(get_parent())
		#sticked_cards_list.append(get_parent())
		
		for i in range(cards_to_stick.size()):
			if (
				i <= cards_to_stick.size() - 1 and
				card_idx + cards_to_stick[i] >= 0 and
				card_idx + cards_to_stick[i] <= card_list.get_child_count() - 1 and 
				not card_list.get_children()[card_idx + cards_to_stick[i]] is StartCard
			):
				print("grudou numa carta!")
				sticked_cards_list.append(card_list.get_children()[card_idx + cards_to_stick[i]])
				cards_to_stick.remove_at(i)
		
		# updates the sticked_cards_list so that every sticked card has an updated list of all
		# cards sticked to it
		print("current sticked cards: " + str(sticked_cards_list))
		for sticked_card in sticked_cards_list: 
			# check if node path is right
			if not sticked_card.has_node("StickedState"):
				var sticked_state_node = sticked_state.instantiate()
				
				sticked_state_node.sticked_cards_list = sticked_cards_list
				sticked_card.add_child(sticked_state_node)
			else:
				sticked_card.get_node("StickedState").sticked_cards_list = sticked_cards_list

		#if cards_to_stick.is_empty():
		_deactivate()
			
		print("sticked cards:", sticked_cards_list)


# maybe in the future make a function to deactivate an effect
func _deactivate():
		PlaceCardRule.get_place_card_node(self).entered_timeline.disconnect(_try_activate)

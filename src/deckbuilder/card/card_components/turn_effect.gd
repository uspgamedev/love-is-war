extends Node


signal effect_enabled

@export var turns_to_emit: int = 1

var turns_passed: int = 0


func _ready():
	%TurnManager.connect("turn_changed", try_effect)
	

func try_effect():
	turns_passed += 1
	if turns_passed == turns_to_emit:
		emit_signal("effect_enabled")

extends EnablerComponent


var turns_to_emit: int = 1
var turns_passed: int = 0


func _ready():
	TurnManager.get_turn_manager_node(self).turn_changed.connect(_try_effect)
	

func _build(parameters: Dictionary):
	turns_to_emit = parameters.get("turns_to_emit", 0)


func _try_effect():
	turns_passed += 1
	if turns_passed == turns_to_emit:
		emit_signal("effect_enabled")	
		turns_passed = 0

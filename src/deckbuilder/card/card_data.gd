class_name CardData
extends Resource

enum CARD_CLASS {
	EXPRESS, # expressar
	ACCEPT, # acolher
	UNWIND, # descontrair
}


@export var card_name: String
@export var card_class: CARD_CLASS
@export var card_description: String
## what type?
#@export var components
@export var self_esteem_cost: int
@export var intimacy_level: int

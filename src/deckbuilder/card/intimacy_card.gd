class_name IntimacyCard
extends Card
## Card used to increase the overall intimacy level during a [Battle].
## Able to merge with other [IntimacyCard]s of the same level.

@export_file("*.png") var intimacy_icon_texture

## Intimacy level of this card
var intimacy_level: int = 1

## Reference to intimacy icon to be displayed on the card 
@onready var level_display: HBoxContainer = $Template/Front/VBoxContainer/HBoxContainer


# Called when the node enters the scene tree for the first time.
func _ready():
	display_level()


## Displays as many intimacy icons on the card as it's level
func display_level():
	for i in intimacy_level:
		var intimacy_icon: TextureRect = TextureRect.new()
		intimacy_icon.texture = intimacy_icon_texture
		level_display.add_child(intimacy_icon)


func is_match(matching_card: Card) -> bool:
	if (
			matching_card is IntimacyCard 
			and intimacy_level == matching_card.intimacy_level 
			and intimacy_level < 3
	):
		return true
	return false

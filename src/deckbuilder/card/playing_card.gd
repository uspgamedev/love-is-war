class_name PlayingCard
extends Card
## Card used by the [Player] and [Date] to manipulte the [Timeline] during a [Battle]. 
## Stores information about the card costs, effects and it's appearance.

## Card "nickname" used as a key in the set definition.
var _key: String
## Information to display on the [CardFront].
var _info: Dictionary
## Reference to the owner of the card, either the [Player] or the [Date]
var card_owner: Character


## Initializes the necessary information to populate the [CardFront].
## Uses the [code]card_key[/code] to extract approppriate info from a [code]card_set[/code] containing several cards.
## Returns new [PlayingCard].
func _new(card_key: String, card_set: Dictionary, c_owner: Character) -> PlayingCard:
	_info = card_set[card_key].duplicate()
	_key = card_key
	card_owner = c_owner
	
	# for componeni in card_set[card_key]:
	
	return self


func _ready():
	card_front.populate_content()
	build_effects()
	

func is_match(matching_card: Card) -> bool:
	if matching_card is PlayingCard and card_owner != matching_card.card_owner:
		if _info.Class == "acolher" and matching_card._info.Class == "expressar":
			return true
		if _info.Class == "expressar" and matching_card._info.Class == "acolher":
			return true
		if _info.Class == "descontrair" and matching_card._info.Class == "descontrair":
			return true

	return false


func build_effects():
	for enabler_component in _info.Components:
		var enabler_component_idx = _info.Components[enabler_component]
		var enabler_card_component = enabler_component.instantiate()
		add_child(enabler_card_component)
	
		if enabler_card_component is EnablerComponent:
			enabler_card_component.build(enabler_component_idx["parameters"])
			
			for affected_component in enabler_component_idx:
				if affected_component is PackedScene:
					var affected_component_idx = enabler_component_idx[affected_component]
					var affected_card_component = affected_component.instantiate()
					
					add_child(affected_card_component)
					affected_card_component.build(affected_component_idx["parameters"])
					enabler_card_component.effect_enabled.connect(affected_card_component.activate)

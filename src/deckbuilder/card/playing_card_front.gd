class_name PlayingCardFront
extends NinePatchRect
## Card front template for [PlayingCard]s.
## Handles populating cards with their content and adjusting the layout accordingly.

@export var card_classes_info: Dictionary

## Reference to the [PlayingCard] node associated with this [PlayingCardFront].
@onready var card: PlayingCard = get_node("../../")
## Reference to all the card front template nodes that need to be populated.
@onready var card_nodes : Dictionary = {
	"Name": %Name,
	"Illustration": %Illustration,
	"ClassIcon": %ClassIcon,
	"ClassName": %ClassName,
	"Effects": %Effects,
	"SelfEsteemCost": %SelfEsteemCost,
	"IntimacyCost": %IntimacyCost,
}


## Populates the [code]card_nodes[/code] with content from the parent [PlayingCard]'s [code]_info[/code].
## Returns the populated [PlayingCardFront].
func populate_content() -> void:
	card_nodes.Name.text = card._info.Name
	adjust_name_size()
	
	var new_illustration: Texture = load("res://assets/textures/card_illustrations/" + card._key + ".png")
	var new_stylebox: StyleBoxTexture = card_nodes.Illustration.get_theme_stylebox("panel", "IllustrationPanel").duplicate()
	new_stylebox.texture = new_illustration
	card_nodes.Illustration.add_theme_stylebox_override("panel", new_stylebox)
	
	card_nodes.ClassName.text = card._info.Class
	theme = card_classes_info[card._info.Class].card_class_theme
	texture =  card_classes_info[card._info.Class].card_class_bg_texture
	card_nodes.ClassIcon.texture = card_classes_info[card._info.Class].card_class_icon
	
	card_nodes.Effects.text = card._info.Description
	adjust_description_size()
	
	card_nodes.SelfEsteemCost.text = str(card._info.SelfEsteemCost)
	card_nodes.IntimacyCost.text = str(card._info.IntimacyCost)


## Adjusts card name font size to fit maximum vertical space available.
func adjust_name_size() -> void:
	if card_nodes.Name.get_total_character_count() > 15:
		card_nodes.Name.add_theme_font_size_override("font_size", 24)

	if card_nodes.Name.get_total_character_count() > 40:
		card_nodes.Name.add_theme_font_size_override("font_size", 20)

# TODO: make a more flexible version of this function.
# However, the solution below doesn't work, because the label height doesn't return correctly
# Maybe the two adjustment functions could be the same if the name was also a RichText insteaad of label

#	var font_size : int = 30
#	var height = card_nodes.Name.size.y
#
#	while height > 54:
#		font_size -= 1
#		card_nodes.Name.add_theme_font_size_override("font_size", font_size)
#
#		height = card_nodes.Name.size.y
#		print(card_nodes.Name, card_nodes.Name.text, card_nodes.Name.size.y)


## Adjusts card effects description font size to fit maximum vertical space available.
func adjust_description_size() -> void:
	var font_size : int = 24
	var height = card_nodes.Effects.get_content_height()
	
	while height > 1800: # TODO: figure out where this number actually comes from
		font_size -= 1
		card_nodes.Effects.add_theme_font_size_override("normal_font_size", font_size)
		
		height = await card_nodes.Effects.get_content_height()

class_name StartCard
extends Card
## Card used to win the game by getting to the top of the [Timeline]. 
## TODO: Plays animations when it's unlocked and when it gets to the top.


func _ready():
	location = GameInfo.CardLocations.TIMELINE


func is_match(_matching_card: Card) -> bool:
	return false

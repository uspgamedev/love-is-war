class_name CardContainer
extends Node2D
## General class for containers that organize [Card]s in an ordered list.
## [Hand], [Drawpile] and [Timeline] inherit from this.
## Supports moving [Card]s between containers, pikcing cards or reordering them.


## If true, the cursor is hovering over the [CardContainer]
var hovering: bool = false
## Indicates whether a [CardContainer] can be interacted with.
## If [code]false[/code], it's functionalities are disabled. 
var can_be_interacted_with: bool = true

## Reference to the [CardContainer]'s [DropZone]
@onready var drop_zone: DropZone = $DropZone
## Reference to the [CardContainer]'s [CardList]
@onready var card_list = $CardList


## Moves a chosen [code]card[/code] from this container to a given [code]destinaton[/code] container.
## If [code]card[/code] isn't specified, moves the top card in this container.
func move_card_to(destination: CardContainer, card: Card = get_top_card()) -> void:
	if can_be_interacted_with:
		if card in card_list.get_children():
			remove_card(card)
			destination.add_card(card)
			card.location = GameInfo.CardLocations.HAND


## Moves a chosen [code]card[/code] from a given [code]origin[/code] container to this container.
## If [code]card[/code] isn't specified, moves the top card in the [code]origin[/code] container.
func move_card_from(origin: CardContainer, card: Card = origin.get_top_card()) -> void:
	if can_be_interacted_with:
		if card in origin.card_list.get_children():
			origin.remove_card(card)
			add_card(card)


## Identifies the topmost [Card] in the [CardContainer]'s [code]card_list[/code].
## that is being hoverd by the cursor, and changes it's state to dragging.
## Returns the [Card] that was picked.
func pick_card():
	if can_be_interacted_with:
		var picked_card = get_top_card(card_list.get_children().filter(func(card): return card.is_state("HOVERING")))
		
		if picked_card:
			picked_card.set_state("DRAGGING")
			picked_card.flip(true)
		
		return picked_card


## Returns the top card on a given [code]card_subset[/code]of the container's [Card]s.
## If no [code]card_subset[/code] is specified, returns the top card in the container.
func get_top_card(card_subset: Array = card_list.get_children()):
	if can_be_interacted_with:
		return card_subset.back() if not card_subset.is_empty() else null


## Adds a given [Card] to the container.
func add_card(card: Card) -> void:
	card_list.add_child(card) 


## Inserts a given [Card] to a given position in the container. The [Card]
## is first inserted at the end of the [code]card_list[/code], then reallocated to its  
## If [code]pos[/code] is not specified, [Card] is inserted at the top
## of the container
func insert_card(card: Card, pos: int = -1) -> void:
	card_list.add_child(card)
	if pos != -1:
		pos = clamp(pos, 0, card_list.get_child_count())
		reallocate_card(card_list.get_child_count() - 1, pos)


## Moves the card from position [code]source[/code] to [code]dest[/code]
## in [code]card_list[/code]
func reallocate_card(source: int, dest: int) -> void:
	var source_card = card_list.get_child(source)
	var step = 1 if (source < dest) else -1

	for i in range(source, dest, step):
		var moving_card = card_list.get_child(i + step)
		card_list.move_child(moving_card, i)

	card_list.move_child(source_card, dest)


## Removes a given [Card] from the container.
func remove_card(card: Card, _check_for_match: bool = true) -> void:
	card_list.remove_child(card)


## Clears the container and deletes all it's [Card]s.
func clear() -> void:
	if card_list.get_child_count() > 0:
		for card in card_list.get_children().duplicate():
			remove_card(card)
			card.queue_free()


## Rearranges the [Card]s in the [code]card_list[/code] in a random order 
func shuffle() -> void:
	var last_index = card_list.get_children().size() - 1
	
	randomize()
	while last_index > 0:
		var random_index = randi_range(0, last_index)
		var card_a = card_list.get_child(last_index)
		var card_b = card_list.get_child(random_index)
		card_list.move_child(card_a, random_index)
		card_list.move_child(card_b, last_index)
		last_index -= 1
	# TODO: adjust visibility order if cards are facing up
	# TODO: add shuffle animation


func _on_drop_zone_mouse_entered():
	hovering = true


func _on_drop_zone_mouse_exited():
	hovering = false


# Handles mouse interactions with the container.
# All the below interactions are temporary and should be modified for the final game.
func _on_drop_zone_gui_input(_event):
	if Input.is_action_pressed("right_click"):
		shuffle()

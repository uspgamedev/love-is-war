class_name Drawpile
extends CardContainer
## A type of [CardContainer] that stacks cards vertically and facing down.
## Handles pile setup and interactions.

signal card_drawn

## Reference to the character hand
# TODO make so that the hand can be accessed through code
@export var playing_card: PackedScene
@export var character_hand: Hand 
@onready var game_state: GameState = GameState.get_game_state_node(self)
	
	
## Populates the pile with chosen [code]card_amounts[/code] of each card from the chosen [code]card_set[/code].
## Returns a shuffled list of the piles' cards.
func setup_pile(card_set: Dictionary, card_amounts: Dictionary, deck_owner: Character) -> Array:
	for card in card_set:
		for n in card_amounts[card]:
			var new_card = GameItems.playing_card.instantiate()._new(card, card_set, deck_owner)
			add_card(new_card)
	
	shuffle()
	return card_list.get_children()


# Handles mouse interactions with the container.
# All the below interactions are temporary and should be modified for the final game.
func _on_drop_zone_gui_input(_event):
	if Input.is_action_pressed("right_click"):
		shuffle()
		
	if Input.is_action_just_pressed("left_click"):
		$CardDraw.play() 
		Rulebook.resolve(Effect.new({"draw_card": true, "drawpile": self, "hand": character_hand}), game_state)
		card_drawn.emit()

class_name DropZone
extends Panel
## Class to handle dragging and dropping [Card]s between [CardContainer]s.
## Must always be a child of [CardContainer].

const HIGHLIGHTED_STYLE: StyleBoxFlat = preload("res://assets/themes/HighlightedPanelStylebox.tres")

## if [code]true[/code], the drop zone is currently highlighted
var highlighted: bool = false


# Checks every frame if drop zone should be highlighted.
func _process(_delta):
	highlight_drop_zone()


## Highlights container when it's valid to release the mouse to drop a card.
## Drops are valid if the mouse is over the container and there are overlapping cards being dragged.
func highlight_drop_zone() -> void:
	var has_droppable_card_in_zone = get_parent().get_overlapping_areas().any(
			func(area): return area.is_state("DRAGGING") if area is Card else false
	)
	
	if get_parent().hovering and has_droppable_card_in_zone:
		add_theme_stylebox_override("panel", HIGHLIGHTED_STYLE)
		highlighted = true
	else:
		remove_theme_stylebox_override("panel")
		highlighted = false


## When a container is clicked + drag, picks the appropriate card and stores as drag data.
## Returns the picked card
func _get_drag_data(_at_position: Vector2) -> Variant:
	return get_parent().pick_card()


## Checks wether the drop zone can receive the card.
## For now, always returns true, allowing cards to be dropped anywhere.
func _can_drop_data(_at_position, _data) -> bool:
	return true


## When the mouse is released over a [DropZone] that can receive the stored drag data,
## drops picked card in the parent [CardContainer].
func _drop_data(_at_position, data) -> void:
	get_parent().move_card_from(data.get_parent().get_parent(), data)
	
	if get_parent() is Hand:
		get_parent().place_card()

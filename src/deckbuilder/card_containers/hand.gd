class_name Hand
extends CardContainer
## A type of [CardContainer] that stacks cards elliptically and facing up.
## Handles card visibility adjustments. 


@onready var game_state := GameState.get_game_state_node(self)

#var _enlarge_tween: Tween
#var _reduce_tween: Tween

func _init():
	add_to_group(&"hand")


#Whenever there are card in the hand, readjusts their visibility
func _process(_delta):
	if (
		card_list.get_child_count() != 0 
		and get_parent().can_play
	):
		adjust_card_visibility()


## Draws a chosen amount of cards from a given [DrawPile] to the [Hand].
func draw_cards_from(drawpile: Drawpile, card_amount: int = 1) -> void:
	for n in card_amount:
		move_card_from(drawpile)

## Finds the index to place a card based on the position it was dropped in the [Hand]
## Places the card in that position in the card list
## Returns the index at which the new card was placed
func place_card():
	if get_parent().can_play:
		var place_index: int = card_list.get_children().size() - 1
		
		for card in card_list.get_children():
			if card.position.x >= get_local_mouse_position().x:
				place_index = card_list.get_children().find(card)
				insert_card(card_list.get_children()[-1], place_index)
				break
		
		return place_index


## Calculates each card's position relative to the amount of cards in [Hand].
## Adjusts cards' z-index relative to their index in the [code]card_list[/code].
## Spotlights hovered cards by raising them.
## Ensures cards are always facing up when in [Hand].
func adjust_card_visibility() -> void:
	if get_parent().can_play:
		var cards_in_hand: int = card_list.get_children().size()
		var card_width: float = card_list.get_child(0).card_front.size.x / 2
		var max_hand_width: float = drop_zone.size.x - card_width
		
		var card_gap_max: float = card_width * 1.1 # Maximum distance between cards
		var card_gap_min: float = card_width / 2 # Minimum distance between cards
		
		# The distance between cards is inversely proportional to the amount of cards in hand
		var card_gap: float = max(min(max_hand_width / cards_in_hand, card_gap_max), card_gap_min)
		
		# The distance to offset cards so the hovered card isn't overlapping
		var card_offset: float = card_gap_max - card_gap
		
		# The current width between the centers of the leftmost and rightmost cards
		var hand_width: float = (card_gap * (cards_in_hand - 1))
		
		# Find vector position to place each card in the hand
		for card in card_list.get_children():
			var card_index: int = card_list.get_children().find(card) # Index of current card
			
			var card_position_x: float = (
					card_gap * card_index
					+ card_offset * get_side_to_offset(card_index)
					- hand_width / 2
					)
			
			var card_position_y: float = 0.0
			
			if card.is_state("HOVERING") and not drop_zone.highlighted:
				card_position_y = -0.75 * card_width
				#if _reduce_tween:
					#_reduce_tween.kill()
				#_enlarge_tween = create_tween()
				#_enlarge_tween.tween_property(card, "position", Vector2(card_position_x, card_position_y), 0.2)
							
			card.position = Vector2(card_position_x, card_position_y)
			card.flip(true)
			
			if not card.is_state("HOVERING"):
				#if _enlarge_tween:
					#_enlarge_tween.kill()
				#_reduce_tween = create_tween()
				#_reduce_tween.tween_property(card, "position", Vector2(card_position_x, 0.0), 0.2)
			
				card.z_index = card_index
		

## Determines to which side the card with given [coe]card_index[/code] should be offset
## to spotlight a hovered card, or open space for a dragged card, if there are any.
## Returns left (-1), right (+1) or not at all (0).
func get_side_to_offset(card_index: int):
	if get_parent().can_play:
		if drop_zone.highlighted:
			return signf(card_list.get_child(card_index).position.x - get_local_mouse_position().x)
		else:
			for card in card_list.get_children():
				if card.is_state("HOVERING"):
					return signi(card_index - card_list.get_children().find(card))
		
		return 0


## Just as in [CardContainer] class, but doesn't pick cards which the [Character] cannot play
func pick_card():
	if get_parent().can_play:
		var picked_card = get_hovering_card()
		
		if picked_card != null:
			if (
				picked_card._info.SelfEsteemCost > get_parent().self_esteem_points 
				or picked_card._info.IntimacyCost > get_parent().game_state.intimacy_level
			):
				return  
			
			if picked_card:
				picked_card.set_state("DRAGGING")
				picked_card.flip(true)
			
			return picked_card


func get_hovering_card():
	return get_top_card(card_list.get_children().filter(func(card): return card.is_state("HOVERING")))


func _on_drop_zone_gui_input(_event):
	if Input.is_action_just_pressed("left_click"):
		if get_parent().can_play:
			var card_to_play = get_hovering_card()
			if card_to_play != null:
				Rulebook.resolve(Effect.new({"play_card": true, "card": card_to_play, "card_owner": get_parent()}), game_state)

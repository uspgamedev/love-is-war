class_name Timeline
extends CardContainer
## A type of [CardContainer] that stacks cards elliptically and facing up.
## Handles card visibility adjustments.

var start_card: StartCard

@onready var player := Player.get_player_node(self)
@onready var date := Date.get_date_node(self)
@onready var game_state := GameState.get_game_state_node(self)


func _ready():
	start_card = GameItems.start_card.instantiate()
	add_card(start_card)


# Whenever there are card in the hand, readjusts their visibility
func _process(_delta):
	if card_list.get_child_count() != 0:
		adjust_card_visibility()
	

func move_card_from(origin: CardContainer, card: Card = origin.get_top_card()) -> void:
	# maybe not used anymore
	if can_be_interacted_with:
		if card in origin.card_list.get_children():
			
			origin.remove_card(card)
			Rulebook.resolve(Effect.place_card(card), game_state)
			# just to be sure, since cards on hand are always PlayingCards
			if card is PlayingCard:
				card.card_owner.update_self_esteem(-card._info.SelfEsteemCost)
				if card._info.SelfEsteemCost > 0:
					card.card_owner.has_spent_points = true


func find_card(pos: int) -> Card:
	pos = clamp(pos, 0, card_list.get_child_count() - 1)
	return card_list.get_child(pos)


## inserts card as in [CardContainer], but takes a third argument [code]check_for_match[/code] to
## call method [code]check_match()[/code]
func insert_card(card: Card, pos: int = -1, check_for_match: bool = true) -> void:
	card_list.add_child(card)
	card.location = GameInfo.CardLocations.TIMELINE
	if pos != -1:
		pos = clamp(pos, 0, card_list.get_child_count())
		reallocate_card(card_list.get_child_count() - 1, pos, false)

	if check_for_match:
		check_match()


## removes card as in [CardContainer], but takes a third argument [code]check_for_match[/code] to
## call method [code]check_match()[/code]
func remove_card(card: Card, check_for_match: bool = true) -> void:
	card_list.remove_child(card)

	if check_for_match:
		check_match()


## adds card as in [CardContainer], but takes a third argument [code]check_for_match[/code] to
## call method [code]check_match()[/code]
func add_card(card: Card, check_for_match: bool = true) -> void:
	card_list.add_child(card)

	if check_for_match:
		check_match()


## reallocates card as in [CardContainer], but takes a third argument [code]check_for_match[/code] to
## call method [code]check_match()[/code]
func reallocate_card(source: int, dest: int, check_for_match: bool = true) -> void:
	var source_card = card_list.get_child(source)

	if source != dest:
		var step = 1 if (source < dest) else -1
		
		dest = clamp(dest, 0, card_list.get_child_count() - 1)

		for i in range(source, dest, step):
			var moving_card = card_list.get_child(i + step)
			card_list.move_child(moving_card, i)

		card_list.move_child(source_card, dest)

	if check_for_match:
		check_match()


## Calculates each card's position relative to the amount of cards in [Hand].
## Adjusts cards' z-index relative to their index in the [code]card_list[/code].
## Ensures cards are always facing up when in [Hand].
func adjust_card_visibility() -> void:
#	var cards_in_timeline: int = card_list.get_children().size()
	var card_width: float = card_list.get_child(0).card_front.size.x / 2
	var timeline_width: float = drop_zone.size.x - card_width
	var card_gap: float = card_width / 2 # Minimum distance between cards
	
	# The distance to offset cards so the hovered card isn't overlapping
	var card_offset: float = card_width * 1.1 - card_gap
	
	# Find vector position to place each card in the hand
	for card in card_list.get_children():
		var card_index: int = card_list.get_children().find(card) # Index of current card
		
		var card_position_x: float = (
				card_gap * card_index
				+ card_offset * int(should_offset(card_index))
				- timeline_width / 2
				)
		
		card.position = Vector2(card_position_x, 0.0)
		card.flip(true)
		
		if not card.is_state("HOVERING"):
			card.z_index = card_index


## Returns true if the card with given [code]card_index[/code] should be offset
## to spotlight a hovered card, if there is any.
func should_offset(card_index: int) -> bool:
	for card in card_list.get_children():
		if card.is_state("HOVERING") and card_index > card_list.get_children().find(card):
			return true
	
	return false


## Checks for matches in [Timeline], stores them in an array and modifies [code]card_list[/code], preparing
## for the matches to be settled
func check_match() -> void:
	# stores the position of the card closer to the start of the pile
	var timeline_cards = card_list.get_children()
	var i: int = 0

	while i < len(timeline_cards) - 1:
		if timeline_cards[i].is_match(timeline_cards[i+1]):
			#remove_card(timeline_cards[i+1], false)
			#match_queue.append(i)
			if (
				timeline_cards[i] is IntimacyCard or 
				timeline_cards[i]._info.Class != "descontrair" 
			): 
				#print(" cartas ", i, " e ", i+1)
				Rulebook.resolve(Effect.match_cards(i, i+1), game_state)
			else:
				print("gargalhadas nas cartas ", i, " e ", i+1)
				Rulebook.resolve(Effect.laughter(i, i+1), game_state)
		i += 1
		# updates card_list reference
		timeline_cards = card_list.get_children()

	#settle_match(match_queue)


## Settles matches checked by [code]check_match()[/code] according to the type of match,
## making all the necessary changes to [code]card_list[/code]
func settle_match(match_queue: Array[int]) -> void:
	match_queue.reverse()
	for i in match_queue:
		var matching_card = card_list.get_child(i)
		
		if matching_card is IntimacyCard or matching_card._info.Class == "acolher" or matching_card._info.Class == "expressar":
			var intimacy_card = GameItems.intimacy_card.instantiate()
		
			if matching_card is IntimacyCard:
				intimacy_card.intimacy_level = matching_card.intimacy_level + 1

			# TODO fix this  
#			if intimacy_card.intimacy_level > game_state.intimacy_level:
#				game_state.update_intimacy_level(intimacy_card.intimacy_level)

			var pos = card_list.get_children().find(start_card)

			if i != match_queue[0]:
				remove_card(matching_card, false)
				insert_card(intimacy_card, i, false)
				reallocate_card(pos, clamp(pos + intimacy_card.intimacy_level, 0, card_list.get_child_count() - 1), false)
			# if it's the last match of the batch to be settled,
			# then another batch of matches can be checked
			else:
				remove_card(matching_card, false)
				insert_card(intimacy_card, i, false)
				reallocate_card(pos, clamp(pos + intimacy_card.intimacy_level, 0, card_list.get_child_count() - 1))

		# gargalhadas
		else:
			if i != match_queue[0]:
				remove_card(matching_card, false)
			else:
				remove_card(matching_card)
			player.update_self_esteem(1)
			date.update_self_esteem(1)

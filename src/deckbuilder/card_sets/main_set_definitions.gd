const SELF_ESTEEM: String = " [img]res://assets/textures/ui/autoestima_icon.png[/img] "
const INTIMACY: String = " [img]res://assets/textures/ui/intimidade_icon.png[/img] "

const SET = "Main Set"
var CARDS : Dictionary = {
	"bom_dia": {
		"Name": "Dar bom dia",
		"Class": "descontrair",
		"Description": "Ganhe +1" + SELF_ESTEEM + "por rodada enquanto essa carta estiver na mesa",
		"Effect": "bla",
		# list all components this card has
		# for every EffectComponent, there is an EnablerComponent that dictates when an effect will be 
		# activated. And for every EffectComponent, information has to be given regarding the parameters
		# it needs to be build (differs for each EffectComponent)
		"Components": {
			GameItems.turn_effect_component: {
				"parameters": {
					"turns_to_emit": 2,
				},
				GameItems.give_self_esteem_component: {
					"parameters": {
						"activation_location": GameInfo.CardLocations.TIMELINE,
						"amount": 1, 
					},
				},
			},
		},
		"SelfEsteemCost": 0,
		"IntimacyCost": 0,
	},
	"segurar_mao": {
		"Name": "Segurar a mão",
		"Class": "expressar",
		"Description": "Gruda nas duas cartas ao lado até alguma delas fazer match",
		"Effect": "bla",
		"Components": {
			GameItems.enter_location_effect_component: {
				"parameters": {
					"enter_location": GameInfo.CardLocations.TIMELINE,
				},
				GameItems.stick_on_component: {
					"parameters": {
						"activation_location": GameInfo.CardLocations.TIMELINE,
						"cards_to_stick": [-1, 0, 1],
					},
				},
			},
		},
		"SelfEsteemCost": 2,
		"IntimacyCost": 0,
	},
	"bilhetinho": {
		"Name": "Entregar bilhetinho feito à mão",
		"Class": "acolher",
		"Description": "Ganhe tantos pontos de" + SELF_ESTEEM + "quanto for o nível de" + INTIMACY,
		"Effect": "bla",
		"Components": {
			GameItems.enter_location_effect_component: {
				"parameters": {
					"enter_location": GameInfo.CardLocations.TIMELINE,
				},
				GameItems.give_varying_self_esteem_component: {
					"parameters": {
						"activation_location": GameInfo.CardLocations.TIMELINE,
						"amount": "intimacy_level",		
					},
				},
			},
		},
		"SelfEsteemCost": 1,
		"IntimacyCost": 0,
	},
	"Montar_Uma_Playlist_Da_Relacao_": {
		"Name": "Montar uma playlist sobre a relação",
		"Class": "expressar",
		"Description": "Elimine duas cartas aleatórias da linha do tempo",
		"Effect": "bla",
		"Components": {
			GameItems.enter_location_effect_component: {
				"parameters": {
					"enter_location": GameInfo.CardLocations.TIMELINE,
				},
				GameItems.remove_cards_component: {
					"parameters": {
						"activation_location": GameInfo.CardLocations.TIMELINE,
						"cards_to_remove": 2,
					},
				},
			},
		},
		"SelfEsteemCost": 2,
		"IntimacyCost": 0,
	},
	"Date_Folgado": {
		"Name": "Date folgado",
		"Class": "acolher",
		"Description": "Compre 1 carta aleatória da mão do seu date",
		"Effect": "bla",
		"Components": {
			GameItems.enter_location_effect_component: {
				"parameters": {
					"enter_location": GameInfo.CardLocations.TIMELINE,
				},
				GameItems.draw_cards_from_hand_component: {
					"parameters": {
						"activation_location": GameInfo.CardLocations.TIMELINE,
						"amount": 1
					},
				},
			},
		},
		"SelfEsteemCost": 2,
		"IntimacyCost": 1,
	},
	"Stalkear_Nas_Redes": {
		"Name": "Stalkear pessoa nas redes",
		"Class": "acolher",
		"Description": "Aumenta intimidade em 1 nível por 3 rodadas",
		"Effect": "bla",
		"Components": {
			GameItems.turn_effect_component: {
				"parameters": {
					"turns_to_emit": 1,
				},
				GameItems.increase_intimacy_component: {
					"parameters": {
						"activation_location": GameInfo.CardLocations.TIMELINE,
						"amount": 1,
						"rounds": 3
					},
				},
			},
		},
		"SelfEsteemCost": 0,
		"IntimacyCost": 0,
	},

#	"carta_longa": {
#		"Name": "Carta com textos extremamente longos que realmente é muito longa",
#		"Illustration": "res://assets/mockups/mockup-card-illustration-acolher.png",
#		"Class": "expressar",
#		"Description": "Essa carta serve pra testar o redimensionamento e ver se
#				realmente ele está conseguindo redimensionar de tal forma que
#				fica um tamanho razoável as cartas que tem um texto tipo assim
#				absurdamente longo como vocês podem ver",
#		"Effect": "bla",
#		"Atributes": "ble",
#		"SelfEsteemCost": 999,
#		"IntimacyCost": 999,
#	},
}

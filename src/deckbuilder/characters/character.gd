class_name Character
extends Node2D


## The amount of self esteem points the character has
var self_esteem_points: int = 5
var self_esteem_max: int = 5
var has_spent_points: bool = false
var can_play: bool = false
var draw_limit: int
@onready var hand := $Hand
@onready var mixed_drawpile := $MixedDrawpile
@onready var unwind_drawpile := $UnwindDrawpile 
@onready var game_state := GameState.get_game_state_node(self)


# Called when the node enters the scene tree for the first time.
func _ready():
	$SelfEsteemCounter.text = str(self_esteem_points) + " / " + str(self_esteem_max)


## Updates character's self esteem points by a given amount
func update_self_esteem (modifier: int) -> void:
	self_esteem_points += modifier

	if self_esteem_points > self_esteem_max:
		self_esteem_points = self_esteem_max

	# not guaranteed it'll happen
	elif self_esteem_points < 0:
		push_warning("invalid state!! -> self_esteem_points < 0")
		self_esteem_points = 0

	# this is just for testing
	$SelfEsteemCounter.text = str(self_esteem_points) + " / " + str(self_esteem_max)

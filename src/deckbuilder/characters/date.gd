class_name Date
extends Character
## the opponent of the [Player] during battle
# Maybe this will be only Gus' script in the end


func _init():
	add_to_group(&"Date")
	
	
func _on_turn_manager_date_turn_started():
	can_play = true
#	hand.can_be_interacted_with = true
#	mixed_drawpile.can_be_interacted_with = true
#	unwind_drawpile.can_be_interacted_with = true
	if not has_spent_points:
		Rulebook.resolve(Effect.change_self_esteem(self, 1), GameState.get_game_state_node(self))
	has_spent_points = false
	draw_limit = 1


func _on_turn_manager_player_turn_started():
	can_play = false
#	hand.can_be_interacted_with = false
#	mixed_drawpile.can_be_interacted_with = false 
#	unwind_drawpile.can_be_interacted_with = false


static func get_date_node(caller: Node) -> Date:
	return caller.get_tree().get_first_node_in_group("Date")

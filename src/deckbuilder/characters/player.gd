class_name Player
extends Character
## The playable character


func _init():
	add_to_group(&"Player")


func _on_turn_manager_player_turn_started():
	can_play = true
#	hand.can_be_interacted_with = true
#	mixed_drawpile.can_be_interacted_with = true
#	unwind_drawpile.can_be_interacted_with = true
	if not has_spent_points:
		Rulebook.resolve(Effect.change_self_esteem(self, 1), GameState.get_game_state_node(self))
		#print("hasn't spent points! regaining...")
	has_spent_points = false
	draw_limit = 1


func _on_turn_manager_date_turn_started():
	can_play = false
#	hand.can_be_interacted_with = false
#	mixed_drawpile.can_be_interacted_with = false
#	unwind_drawpile.can_be_interacted_with = false


static func get_player_node(caller: Node) -> Player:
	return caller.get_tree().get_first_node_in_group("Player")

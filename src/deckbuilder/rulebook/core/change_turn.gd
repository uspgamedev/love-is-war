extends ApplyRule


func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.has("change_turn"):
		var new_turn := effect.attributes.get("new_turn", null) as Character
		if new_turn:
			game_state.turn_manager.set_turn(new_turn) 

extends ModifyRule


func _apply(effect: Effect, _game_state: GameState):
	if effect.attributes.has("draw_card"):
		var hand := effect.attributes.get("hand", null) as Hand
		if hand != null:
			var hand_owner := hand.get_parent()
			if hand_owner:
				if hand_owner.draw_limit <= 0:
					effect.attributes.erase("draw_card")
					effect.attributes.erase("hand")
					effect.attributes.erase("drawpile")
				else:
					hand_owner.draw_limit -= 1

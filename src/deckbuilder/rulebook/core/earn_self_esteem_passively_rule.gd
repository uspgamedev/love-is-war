extends ApplyRule


func _apply(effect: Effect, _game_state: GameState) -> void:
	if effect.attributes.get("earn_self_esteem_passively", false):
		var player := effect.attributes.get("player", null) as Player
		if player:
			player.update_self_esteem(1)

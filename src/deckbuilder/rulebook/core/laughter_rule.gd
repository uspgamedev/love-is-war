extends ApplyRule


func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.get("laughter", false):
		var player := game_state.player
		var date := game_state.date
		var card_to_remove_1 := effect.attributes.get("card_1_position", -1) as int
		var card_to_remove_2 := effect.attributes.get("card_2_position", -1) as int
		if (
			player and
			date
		):
			Rulebook.resolve(Effect.remove_card(game_state.timeline, card_to_remove_1), game_state)
			Rulebook.resolve(Effect.remove_card(game_state.timeline, card_to_remove_2), game_state)	
			player.update_self_esteem(1)
			date.update_self_esteem(1)

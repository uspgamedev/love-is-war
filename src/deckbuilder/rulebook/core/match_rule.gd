extends ApplyRule


# isso conta como apply rule?
func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.has("match"):
		var card_to_remove_1 := effect.attributes.get("card_1_position", -1) as int
		var card_to_remove_2 := effect.attributes.get("card_2_position", -1) as int
		var _check_for_match := effect.attributes.get("check_for_match", true) as bool
		# the idea is: instantiate 2 effects to remove cards, then an effect to add a card anywhere
		# and pass an instance of an intimacy card, then instance an effect to move the start_card 
		var intimacy_card : IntimacyCard = GameItems.intimacy_card.instantiate()
		var matching_card := game_state.timeline.find_card(card_to_remove_1) as Card
		var timeline := game_state.timeline 
		var current_intimacy_level := GameState.get_game_state_node(self).intimacy_level
		if matching_card is IntimacyCard:
			intimacy_card.intimacy_level = 1 + matching_card.intimacy_level
		
		if (
			card_to_remove_1 >= 0 and
			card_to_remove_2 >= 0
		):
			# MAN this is hacky af
			Rulebook.resolve(Effect.remove_card(timeline, card_to_remove_1), game_state)
			Rulebook.resolve(Effect.remove_card(timeline, card_to_remove_2 - 1), game_state)
			Rulebook.resolve(Effect.place_card(intimacy_card, card_to_remove_1), game_state)
			var start_card_idx := timeline.card_list.get_children().find(timeline.start_card) as int
			Rulebook.resolve( 
				Effect.move_card(
					start_card_idx, 
					intimacy_card.intimacy_level
				), 
				game_state
			)
			if intimacy_card.intimacy_level > current_intimacy_level:
				GameState.get_game_state_node(self).update_intimacy_level(intimacy_card.intimacy_level)

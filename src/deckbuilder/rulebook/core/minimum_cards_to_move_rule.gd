extends ModifyRule


# maybe temporary? Maybe permanent? We'll never know
var disabled: bool = false


# removes the attributes regarding moving the start_card from the effect if
# there are 5 or less cards in the timeline
# TODO maybe cancel the entire effect? 
func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.has("move_card") and not disabled:
		var timeline := game_state.timeline
		var source := effect.attributes.get("source", 0) as int
		
		if (
			timeline.find_card(source) is StartCard and
			timeline.card_list.get_child_count() <= 5
		):
			effect.attributes.erase("move_card")
			effect.attributes.erase("source")
			effect.attributes.erase("places")
		else:
			disabled = true		

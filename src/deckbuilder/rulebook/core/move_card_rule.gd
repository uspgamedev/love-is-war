extends ApplyRule


# This rule effectively handles the mechanic of moving cards along the timeline, while
# other scripts just check and modifies effects regarding this mechanic
func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.has("move_card"):
		var timeline := game_state.timeline
		var source := effect.attributes.get("source", -1) as int
		var places := effect.attributes.get("places", 0) as int
		var check_for_match := effect.attributes.get("check_for_match", true) as bool
		
		if (
			places > 0 and
			source >= 0
		): 
			timeline.reallocate_card(source, source + places, check_for_match)

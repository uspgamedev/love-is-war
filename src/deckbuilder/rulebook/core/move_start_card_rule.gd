extends ApplyRule


# This rule effectively handles the mechanic of moving cards along the timeline, while
# other scripts just check and modifies effects regarding this mechanic
func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.get("move_card", false):
		var timeline := game_state.timeline
		var source := timeline.card_list.find(timeline.start_card) as int
		var places := effect.attributes.get("places", 0) as int
		
		if places > 0: 
			timeline.reallocate_card(source, source + places)

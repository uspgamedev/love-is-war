class_name PlaceCardRule
extends ApplyRule


## Indicates when a card enters the timeline for the EnablerComponent
signal entered_timeline


func _init():
	add_to_group(&"PlaceCardRule")
	
	
func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.get("place_card", false):
		var timeline := game_state.timeline
		var card := effect.attributes.get("card", null) as Card
		var position := effect.attributes.get("position", -1) as int
		var check_for_match := effect.attributes.get("check_for_match", true) as bool
		if (
			card
			and timeline
		):
			emit_signal("entered_timeline", card)
			card.played.emit()
			timeline.insert_card(card, position, check_for_match)


static func get_place_card_node(caller: Node) -> PlaceCardRule:
	return caller.get_tree().get_first_node_in_group(&"PlaceCardRule")

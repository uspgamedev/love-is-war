extends ModifyRule


func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.has("play_card"):
		var card := effect.attributes.get("card", null) as PlayingCard
		var card_owner := effect.attributes.get("card_owner", null) as Character
		if (
			card != null and	
			card._info.SelfEsteemCost <= card_owner.self_esteem_points and
			card._info.IntimacyCost <= game_state.intimacy_level
		):
			$CardPlay.play ()
			var card_cost = card._info.SelfEsteemCost
			
			Rulebook.resolve(Effect.change_self_esteem(card_owner, -card_cost), game_state)
			if card_cost > 0:
				card_owner.has_spent_points = true
			Rulebook.resolve(Effect.remove_card(card_owner.hand, -1, card), game_state)
			Rulebook.resolve(Effect.place_card(card), game_state)

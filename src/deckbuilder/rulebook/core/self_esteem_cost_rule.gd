extends ApplyRule


func _apply(effect: Effect, _game_state: GameState):
	if effect.attributes.get("spend_self_esteem", 0):
		var card_owner := effect.attributes.get("card_owner", null) as Character
		var cost := effect.attributes.get("self_esteem_cost", 0) as int
		if(
			card_owner and 
			cost > 0
		):
			card_owner.update_self_esteem(-cost)

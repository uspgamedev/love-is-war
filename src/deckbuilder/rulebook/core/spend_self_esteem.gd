extends ApplyRule


func _apply(effect: Effect, _game_state: GameState):
	if effect.attributes.has("change_self_esteem"):
		var card_owner := effect.attributes.get("character", null) as Character
		var cost := effect.attributes.get("amount", 0) as int
		if(
			card_owner and 
			cost < 0
		):
			card_owner.update_self_esteem(cost)
			card_owner.has_spent_points = true

extends ModifyRule
## checks if a base win condition is met. If it is, creates a "win" effect that is effectively activated
## when it reaches the [code]WinConditionRule[/code]. Note that this "win" effect can be deleted by other
## [ModifyRule]s.

func _apply(effect: Effect, game_state: GameState):
	var start_card_pos := game_state.timeline.card_list.get_children().find(game_state.timeline.start_card) as int
	
	if effect.attributes.has("check_win"):
		# checking standard win cndition
		var win_candidate := effect.attributes.get("win_candidate", null) as Character
		if (
			start_card_pos != 0
			and game_state.timeline.card_list.get_child_count() - 1 == start_card_pos 
			and win_candidate
		):
			Rulebook.resolve(Effect.win(win_candidate), game_state)

extends ApplyRule
## This rule checks for victory everytime an effect is applied. It does so by
## creating another effect to check for win, for it to be then evaluated for the 
## [code]ValidateWin[\code] rule.

signal won(character: Character)


func _apply(effect: Effect, game_state: GameState):
	var win_candidate := effect.attributes.get("win_candidate", null) as Character 
	if effect.attributes.has("win"):
		emit_signal("won", win_candidate)
	elif effect.attributes.has("check_win"):
		pass 
	else:
		Rulebook.resolve(Effect.check_win(game_state.turn_manager.turn), game_state)
		

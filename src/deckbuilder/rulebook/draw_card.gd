class_name DrawCardRule
extends ApplyRule


## Indicates when a card enters the hand for the EnablerComponent
signal entered_hand


func _init():
	add_to_group(&"DrawCardRule")
	
	
func _apply(effect: Effect, _game_state: GameState):
	if effect.attributes.has("draw_card"):
		var drawpile := effect.attributes.get("drawpile", null) as Drawpile
		var hand := effect.attributes.get("hand", null) as Hand 
		if (
			drawpile and 
			hand
		):
			drawpile.move_card_to(hand)
			emit_signal("entered_hand", drawpile.get_top_card())


static func get_draw_card_node(caller: Node) -> DrawCardRule:
	return caller.get_tree().get_first_node_in_group("DrawCardRule")

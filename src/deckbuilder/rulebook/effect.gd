class_name Effect
extends Node
## Class that represents a stream of attributes that want to
## be applied to the game, changing it's state. Effects must be matched
## with rules active at the given moment to be applied
var attributes := {}


func _init(initial_attributes: Dictionary):
	attributes = initial_attributes.duplicate()


static func place_card(card: Card, position: int = -1, check_for_match: bool = true):
	if position >= 0:
		return Effect.new({
			"place_card": true, 
			"card": card, 
			"position": position, 
			"check_for_match": check_for_match,
		})
	else:
		return Effect.new({
			"place_card": true, 
			"card": card,
			"check_for_match": check_for_match,
		})


static func move_card(source: int, places: int, check_for_match: bool = true):
	return Effect.new({
		"move_card": true, 
		"source": source, 
		"places": places, 
		"check_for_match": check_for_match,
	})


## Creates effect for removing a card either by position on the timeline, 
## or by giving a direct reference to the card to be removed
static func remove_card(
	origin: CardContainer, 
	position: int = -1, 
	card: Card = null, 
	check_for_match: bool = true
):
	if card == null:
		return Effect.new({
			"remove_card": true, 
			"origin": origin, 
			"position": position,
			"check_for_match": check_for_match,
		})
	return Effect.new({
		"remove_card": true, 
		"origin": origin, 
		"card": card,
		"check_for_match": check_for_match,
	})


static func match_cards(card_1_pos: int, card_2_pos: int, check_for_match: bool = true):
	return Effect.new({
		"match": true, 
		"card_1_position": card_1_pos, 
		"card_2_position": card_2_pos,
		"check_for_match": check_for_match,
	})


static func laughter(card_1_pos: int, card_2_pos: int):
	return Effect.new({"laughter": true, "card_1_position": card_1_pos, "card_2_position": card_2_pos})


static func spend_self_esteem(card_owner: Character, self_esteem_cost: int = 0):
	return Effect.new({"spend_self_esteem": true, "card_owner": card_owner, "self_esteem_cost": self_esteem_cost})


static func change_turn(new_turn: Character):
	return Effect.new({"change_turn": true, "new_turn": new_turn})	


static func check_win(win_candidate: Character):
	return Effect.new({"check_win": true, "win_candidate": win_candidate})
	

static func win(win_candidate: Character):
	return Effect.new({"win": true, "win_candidate": win_candidate})
	
	
static func change_self_esteem(character: Character, amount: int):
	return Effect.new({"change_self_esteem":true, "character": character, "amount": amount})

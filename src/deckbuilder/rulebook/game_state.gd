class_name GameState
extends Node
## Class that represent the state of the game at a given moment.
## I.e. all the cards with active effects, how many self-esteem 
## points each player has and so on. Effects matched by rules are responsible
## for changing the state of the game

# NOTE TO SELF: what aspects of the game are relevant for the GameState to keep track?
# 1 - list with all cards in timeline, in order. Important to make a match_rule
# 2 - number of cards in timeline. Important to make a minimum_cards_in_timeline rule
# 3 - number of cards in each character's hands. Important to make rules that limit that number
# 4 - self-esteem points max and current of each player. Important to make rules that limit self-esteem points acquisition 
signal effect_resolved(effect: Effect)

#@export var card_set_file: GDScript = load("res://src/card_sets/main_set_definitions.gd")
@export var card_set_file: GDScript
@export var intimacy_level_label: Label

var counters: Array[int] = []
var intimacy_level := 0

@onready var timeline := %Timeline
@onready var player := %Player
@onready var date := %Date
@onready var timeline_card_list = %Timeline.card_list
@onready var turn_manager := %TurnManager


func _init():
	add_to_group(&"GameState")
	
	
func _ready():
	set_decks()
 

func set_decks():
	# actually sets both decks, will be changed in the future
	var player_deck = {}
	var card_set = card_set_file.new().CARDS
	for key in card_set.keys():
		if card_set[key].Class != "descontrair":
			player_deck[key] = 10
		else: player_deck[key] = 0
	player.get_node("MixedDrawpile").setup_pile(card_set, player_deck, player)
	player_deck = {}
	for key in card_set.keys():
		if card_set[key].Class == "descontrair":
			player_deck[key] = 30
		else: player_deck[key] = 0
	player.get_node("UnwindDrawpile").setup_pile(card_set, player_deck, player)
	
	var date_deck = {}
	for key in card_set.keys():
		if card_set[key].Class != "descontrair":
			date_deck[key] = 10
		else: date_deck[key] = 0
	date.get_node("MixedDrawpile").setup_pile(card_set, date_deck, date)
	date_deck = {}
	for key in card_set.keys():
		if card_set[key].Class == "descontrair":
			date_deck[key] = 30
		else: date_deck[key] = 0
	date.get_node("UnwindDrawpile").setup_pile(card_set, date_deck, date)


func update_intimacy_level(new_intimacy_level: int):
	intimacy_level = clamp(new_intimacy_level, 0, 3)
	intimacy_level_label.set_text(str(intimacy_level))
	print("intimacy level updated! Now: " + str(intimacy_level))


static func get_game_state_node(_caller: Node) -> GameState:
	return Engine.get_main_loop().get_first_node_in_group(&"GameState")

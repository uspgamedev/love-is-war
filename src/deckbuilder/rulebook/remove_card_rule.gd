extends ApplyRule
## Removes [Card] from a [CardContainer] by position or by the card itself

func _apply(effect: Effect, game_state: GameState):
	if effect.attributes.has("remove_card"):
		var origin := effect.attributes.get("origin", null) as CardContainer
		var position := effect.attributes.get("position", -1) as int
		var card := effect.attributes.get("card", null) as Card
		var check_for_match := effect.attributes.get("check_for_match", false) as bool 
		if (
			position >= 0 
			and origin
		):
			if origin is Timeline:
				var card_to_remove = origin.find_card(position)
				var intimacy_values = find_biggest_intimacy_card(origin)
				if card_to_remove is IntimacyCard:
					if card_to_remove.intimacy_level == intimacy_values[0] and card_to_remove.intimacy_level != intimacy_values[1]:
						game_state.update_intimacy_level(intimacy_values[1])
				origin.remove_card(origin.find_card(position), check_for_match)
			else:
				origin.remove_card(origin.find_card(position))
		elif card:
			if origin is Timeline:
				var intimacy_values = find_biggest_intimacy_card(origin)
				if card is IntimacyCard:
					if card.intimacy_level == intimacy_values[0] and card.intimacy_level != intimacy_values[1]:
							game_state.update_intimacy_level(intimacy_values[1])
			origin.remove_card(card)
			
			
func find_biggest_intimacy_card(timeline: Timeline) -> Array[int]:
	var max_values := [0, 0] as Array[int]
	for card in timeline.card_list.get_children():
		if card is IntimacyCard:
			if card.intimacy_level > max_values[0]:
				max_values[0] = card.intimacy_level
			elif card.intimacy_level > max_values[1]:
				max_values[1] = card.intimacy_level
	return max_values
	

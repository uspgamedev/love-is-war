class_name Rule
extends Node
## Superclass for all the rules in the game. Effects that want to be 
## applied to the game state need to be matched by a rule.


# rule groups
const MODIFY_RULE := "MODIFY_RULE"
const APPLY_RULE := "APPLY_RULE"

#var 

func apply(effect: Effect, game_state: GameState):
	return self._apply(effect, game_state)


func _apply(_effect: Effect, _game_state: GameState):
	push_error("Virtual Method _apply")
	return null

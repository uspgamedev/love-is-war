extends Node
## The Rulebook class is responsible for matching the effects that want to 
## be applied to the game state to the rules ruling at a given moment.
## If an effect matches to a rule, it's applied, changing the game state.
## Else, it's discarded 


const RULE_ORDER := [Rule.MODIFY_RULE, Rule.APPLY_RULE]

var effect_stream: Array[Effect] = []


func resolve(effect: Effect, game_state: GameState) -> Effect:
	for rule_group in RULE_ORDER:
		for rule in get_tree().get_nodes_in_group(rule_group):
			#print(rule)
			if rule is Rule:
				rule.apply(effect, game_state)
	game_state.effect_resolved.emit(effect)
	return effect


func preview(effect: Effect, game_state: GameState) -> Effect:
	for rule in get_tree().get_nodes_in_group(Rule.MODIFY_RULE):
		if rule is Rule:
			rule.handle(effect, game_state)
	return effect

extends ModifyRule


func _apply(effect: Effect, _game_state: GameState):
	if effect.attributes.has("place_card"):
		if not effect.attributes.has("position"):
			effect.attributes["position"] = -1

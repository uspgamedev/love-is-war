extends ApplyRule


var lock_rule: bool = false


# TODO remake this rule to avoid infinite recursion without relying on a 
# variable to disable this rule's effect
# receives a signal when a card of the sticked group moves?
# checks if the moving card is sticked?	
func _apply(effect: Effect, game_state: GameState):
	# is this the best way??
	if (
		effect.attributes.has("move_card") or
		effect.attributes.has("remove_card") 
	):
		var card: Card		
		if effect.attributes.has("move_card"):
			card = game_state.timeline.find_card(effect.get("source"))
		if effect.attributes.has("remove_card"):
			if effect.attributes.has("card"):
				card = effect.attributes.get("card")
			else:
				card = game_state.timeline.find_card(effect.attributes.get("position"))
			
		if (
			card and
			card.has_node("StickedState")
		):	
			var sticked_cards_list = card.get_node("StickedState").sticked_cards_list
						
			if not lock_rule:
				lock_rule = true
				for sticked_card in sticked_cards_list:
					var sticked_card_idx = game_state.timeline.card_list.get_children().find(sticked_card)
					
					if effect.attributes.has("move_card"):
						Rulebook.resolve(Effect.move_card(sticked_card_idx, effect.get("places")), game_state) 
					
					if effect.attributes.has("remove_card"):
						Rulebook.resolve(Effect.remove_card(game_state.timeline, -1, sticked_card), game_state)
				lock_rule = false

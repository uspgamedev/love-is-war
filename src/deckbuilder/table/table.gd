class_name Table
extends Node2D
## Controls everything related to a match of the card game, both visual
## and logical.


#@export var card_set_file: GDScript = load("res://src/card_sets/main_set_definitions.gd")
#@export var player: Character
#@export var date: Character
#@export var timeline: Timeline
#
#
#var intimacy_level: int = 0
## these two below are temporary 
#var player_deck: Dictionary
#var date_deck: Dictionary
#
##
##@onready var protagonist_mixed_drawpile: Drawpile = $ProtagonistMixedDrawpile
##@onready var protagonist_unwind_drawpile: Drawpile = $ProtagonistUnwindDrawpile
##@onready var date_mixed_drawpile: Drawpile = $DateMixedDrawpile
##@onready var date_unwind_drawpile: Drawpile = $DateUnwindDrawpile
##@onready var player_hand: Hand = $PlayerHand
##@onready var date_hand: Hand = $DateHand
##@onready var player_discard_pile: DiscardPile = $PlayerDiscardPile
##@onready var date_discard_pile: DiscardPile = $DateDiscardPile
##@onready var turn_manager = $TurnManager
##@onready var card_set: Dictionary = card_set_file.new().CARDS
#
#
## Called when the node enters the scene tree for the first time.
#func _ready():
#	# these two functions below are just for testing
##	set_player_deck()
##	set_date_deck()
#	pass
#	# generates drawpiles off a hypotethical deck.
#	# TODO scramble cards from a real deck, stored in some place
#
#
#func set_player_deck():
#	# actually sets both decks, will be changed in the future
#	player_deck = {}
#	for key in card_set.keys():
#		if card_set[key].Class != "descontrair":
#			player_deck[key] = 10
#		else: player_deck[key] = 0
#	$CanvasLayer/Player.get_node("MixedDrawpile").setup_pile(card_set, player_deck, $CanvasLayer/Player)
#	player_deck = {}
#	for key in card_set.keys():
#		if card_set[key].Class == "descontrair":
#			player_deck[key] = 30
#		else: player_deck[key] = 0
#	$CanvasLayer/Player.get_node("UnwindDrawpile").setup_pile(card_set, player_deck, $CanvasLayer/Player)
#
#	date_deck = {}
#	for key in card_set.keys():
#		if card_set[key].Class != "descontrair":
#			date_deck[key] = 10
#		else: date_deck[key] = 0
#	$CanvasLayer/Date.get_node("MixedDrawpile").setup_pile(card_set, date_deck, $CanvasLayer/Date)
#	date_deck = {}
#	for key in card_set.keys():
#		if card_set[key].Class == "descontrair":
#			date_deck[key] = 30
#		else: date_deck[key] = 0
#	$CanvasLayer/Date.get_node("UnwindDrawpile").setup_pile(card_set, date_deck, $CanvasLayer/Date)
#
#
#func set_date_deck():
#	pass
#
#
#func update_intimacy_level(new_intimacy_level: int) -> void:
#	intimacy_level = new_intimacy_level
#	$CanvasLayer/IntimacyLevel.text = str(intimacy_level)

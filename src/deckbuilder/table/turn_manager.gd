class_name TurnManager
extends Node
## Manages turns, emitting a signal with the information of who's now 
## playing every time the turn changes

#signal turn_changed  
signal player_turn_started
signal date_turn_started
signal turn_changed

enum {PLAYER_TURN, DATE_TURN}

const BASE_TEXT = "'s turn"

@export var game_state: GameState
@export var player: Player
@export var date: Date

var turn: Character : set = set_turn

# testing only
@onready var label = $Label
# TODO: change Date's name to Date
#@onready var whose_turn = get_node("CanvasLayer/Player")


func _ready():
	#change_turn()
	set_turn(player)


## Sets [code]turn[/code] and emits a signal indicating whose turn is now
func set_turn(new_turn: Character):
	turn = new_turn
	match new_turn:
		player: 
			emit_signal("player_turn_started") 
			emit_signal("turn_changed")
		date: 
			emit_signal("date_turn_started")
			emit_signal("turn_changed")
	update_label()


func update_label() -> void:
	match turn:
		date: 
			label.text = "Date" + BASE_TEXT
		player: 
			label.text = "Player" + BASE_TEXT


### Changes turn and emits a signal to indicate who is playing now
#func change_turn() -> void:
#	if whose_turn is Player:
#		whose_turn = get_node("CanvasLayer/Date")
#	else:
#		whose_turn = get_node("CanvasLayer/Player")
#	update_label()
#	emit_signal("turn_changed", whose_turn)


func _on_button_pressed():
	match turn:
		player: 
			Rulebook.resolve(Effect.change_turn(date), game_state)
			#set_turn(DATE_TURN)	
		date:
			Rulebook.resolve(Effect.change_turn(player), game_state)	 
			#set_turn(PLAYER_TURN)


static func get_turn_manager_node(caller: Node):
	return caller.get_tree().get_first_node_in_group("TurnManager")

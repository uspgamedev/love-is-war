extends Label


func _on_win_condition_rule_won(character):
	self.visible = true
	if character is Player:
		text = "You Win"
	else:
		text = "You Lose"

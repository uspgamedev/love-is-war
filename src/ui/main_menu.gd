extends Control


@export var menu_backgrounds: Array

@onready var options = %OptionsMenu
@onready var main = %MenuContainer

func _ready():
	randomize()
	%Background.texture = menu_backgrounds.pick_random()
	options.back.connect(settings_callback)
	SceneManager.current_scene_root = self

## BUTTON PRESSES ##

## New Game Button
func _on_new_game_pressed():
	SceneManager.go_to_introduction()

## Continue Button
func _on_continue_pressed():
	GameInfo.load_game()

## Options Button
func _on_options_pressed():
	change_settings()

## Quit Button
func _on_quit_pressed():
	get_tree().quit()

## OPTIONS MENU ##
func change_settings():
	main.hide()
	options.show()

func settings_callback():
	main.show()
	options.hide()


func _on_bg_change_timeout():
	%Background.texture = menu_backgrounds.pick_random()

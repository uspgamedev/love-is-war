extends Control

signal back

func _ready():
	%Sound_Volume.value = GameInfo.sound_volume
	%Music_Volume.value = GameInfo.music_volume

## Save the volume
func _on_save_pressed():
	GameInfo.save_settings()

## Go back to the main menu
func _on_back_pressed():
	if Dialogic.current_timeline != null:
		Dialogic.Audio.base_sound_player.volume_db = Dialogic.Audio.sound_volume + (GameInfo.sound_volume - 50)
		Dialogic.Audio.base_music_player.volume_db = Dialogic.Audio.music_volume + (GameInfo.music_volume - 50)
	back.emit()

## Change the volume
func _on_sound_volume_scrolling():
	GameInfo.sound_volume = %Sound_Volume.value

func _on_music_volume_scrolling():
	GameInfo.music_volume = %Music_Volume.value

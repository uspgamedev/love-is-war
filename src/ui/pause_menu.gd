extends Control

@onready var main = $"../../"
@onready var options = %OptionsMenu
@onready var pause = %PauseContainer
@onready var relationship_menu = %RelationshipMenu
signal unpause_game
signal quit_game

func _ready():
	unpause_game.connect(GameInfo.pause_handler)
	quit_game.connect(GameInfo.quit_handler)
	options.back.connect(settings_callback)


func _process(delta):
	if Input.is_key_pressed(KEY_ESCAPE) and relationship_menu.visible == true:
		hide_relationship_menu()


## BUTTON PRESSES ##

## Resume
func _on_resume_pressed():
	unpause_game.emit()

## Settings
func _on_options_pressed():
	change_settings()

## Save
func _on_save_pressed():
	GameInfo.save_game()

## Quit
func _on_quit_pressed():
	quit_game.emit()


func _on_relationship_pressed():
	show_relationship_menu()


## SETTINGS ##
func change_settings():
	pause.hide()
	options.show()

func settings_callback():
	options.hide()
	pause.show()


func show_relationship_menu():
	pause.hide()
	relationship_menu.show_menu()


func hide_relationship_menu():
	relationship_menu.hide()
	pause.show()

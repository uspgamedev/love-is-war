extends Control


@onready var relationship_progress_bar = %RelationshipProgressBar
@onready var character_frame = %CharacterFrame


func show_menu():
	visible = true
	relationship_progress_bar.value = 0
	character_frame.texture = null


func _on_gus_pressed():
	show_relationship_information(GameInfo.Characters.GUS)


func _on_bella_pressed():
	show_relationship_information(GameInfo.Characters.BELLA)


func _on_iago_pressed():
	show_relationship_information(GameInfo.Characters.IAGO)


func _on_ana_pressed():
	show_relationship_information(GameInfo.Characters.ANA)


func _on_kiko_pressed():
	show_relationship_information(GameInfo.Characters.KIKO)
	

func show_relationship_information(character: GameInfo.Characters):
	# maybe create a resource with the information for each character and load it in some way
	match character:
		GameInfo.Characters.GUS:
			relationship_progress_bar.value = RelationshipHandler.gus_relationship_level
			character_frame.texture = load("res://assets/characters/gus/chill.png")
		GameInfo.Characters.BELLA:
			relationship_progress_bar.value = RelationshipHandler.bella_relationship_level
			character_frame.texture = load("res://assets/characters/bella/bella_speaking.png")
		_:
			relationship_progress_bar.value = 0
			character_frame.texture = null

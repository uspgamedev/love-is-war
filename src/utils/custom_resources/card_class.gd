extends Resource
class_name CardClass

@export var card_class_name: String
@export var card_class_icon: Texture2D
@export var card_class_theme: Theme
@export var card_class_bg_texture: Texture2D

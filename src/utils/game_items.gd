extends Node


@export var start_card: PackedScene
@export var intimacy_card: PackedScene
@export var playing_card: PackedScene

# EnablerComponents
@export var turn_effect_component: PackedScene
@export var enter_location_effect_component: PackedScene

# EffectComponents
@export var give_self_esteem_component: PackedScene
@export var give_varying_self_esteem_component: PackedScene
@export var stick_on_component: PackedScene
@export var remove_cards_component: PackedScene
@export var draw_cards_from_hand_component: PackedScene
@export var increase_intimacy_component: PackedScene

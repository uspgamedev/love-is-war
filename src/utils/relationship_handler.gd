extends Node


signal relationship_increased(amount: int)
signal relationship_decreased(amount: int)

@export_category("Scenes")
@export var reaction_animations_scene: PackedScene

@export_category("Character variables")
@export var gus_relationship_level: int = 15
@export var bella_relationship_level: int = 3


enum RelationshipLevel {
	LOW = 0,
	MEDIUM = 7,
	HIGH = 15,
	VERY_HIGH = 25,
}


func increase_relationship_level(character: GameInfo.Characters, amount: int):
	match character:
		GameInfo.Characters.GUS:
			gus_relationship_level += amount
		GameInfo.Characters.BELLA:
			gus_relationship_level += amount
	
	var character_portrait = get_target_portrait(character)
	play_relationship_increased(character_portrait)


func decrease_relationship_level(character: GameInfo.Characters, amount: int):
	match character:
		GameInfo.Characters.GUS:
			gus_relationship_level -= amount
		GameInfo.Characters.BELLA:
			gus_relationship_level -= amount

	play_relationship_decreased()


func play_relationship_increased(character_portrait: Node2D):
	var reaction_animation = reaction_animations_scene.instantiate() as AnimatedSprite2D
	character_portrait.add_child(reaction_animation)
	reaction_animation.play(&"love_struck")
	await reaction_animation.animation_finished
	character_portrait.remove_child(reaction_animation)


func play_relationship_decreased():
	# emits signal to play animation
	pass


func get_target_portrait(character: GameInfo.Characters):
	var portraits_node = get_tree().get_first_node_in_group(&"dialogic_layout_default").get_node("VN_PortraitLayer/Portraits")
	var character_node_name = get_character_node_name(character)

	if character_node_name == "":
		push_error("Couldn't get character node name")
		return 
	if not portraits_node:
		push_error("Dialogic portrait not found")
		return
	
	return portraits_node.get_children().map(
		func (x): return x.get_children().filter(
				func (y): return y.name == character_node_name
			).front()
		).filter(
			func (x): return x != null
		).front()


func get_character_node_name(character: GameInfo.Characters) -> String:
	# returns character name as it's written on the node
	return GameInfo.Characters.keys()[character].capitalize()

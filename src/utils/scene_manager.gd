extends Node

@export var main_menu: PackedScene
@export var introduction: PackedScene
@export var tutorial: PackedScene

var current_scene_root: Node

func go_to_main_menu():
	_change_to_scene(main_menu.instantiate())


func go_to_introduction():
	_change_to_scene(introduction.instantiate())


func go_to_tutorial():
	_change_to_scene(tutorial.instantiate())


func _change_to_scene(node: Node):
	current_scene_root.queue_free()
	await current_scene_root.tree_exited
	current_scene_root = node
	get_tree().root.add_child(node)

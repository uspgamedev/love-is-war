class_name SignalBusSender
extends Node


func send_relationship_increased(character: GameInfo.Characters, amount: int):
	SignalBus.send(&'relationship_increased', [character, amount])


func send_relationship_decreased(character: GameInfo.Characters, amount: int):
	SignalBus.send(&'relationship_increased', [character, amount])

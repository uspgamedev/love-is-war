extends Node

var MAXLINE_SIZE = 25


func fade_in(text):
	$AnimationPlayer.play("fade_in")
	
	var j = 0
	for i in text.length():
		if text[i] == ' ' and j>MAXLINE_SIZE:
			text[i] = '\n'
			j = 0
		j += 1
	$FocusBlockout/Label.text = text
	

func fade_out():
	$AnimationPlayer.play("fade_out")

extends Node2D

@export var mixed_drawpile: Drawpile
@export var unwind_drawpile: Drawpile
@export var hand: Hand
@export var game_state: GameState
@export var timeline: Timeline
@export var card_button: Button
@export var mixed_button: Button
@export var unwind_button: Button

@onready var card_focus_blockout: PackedScene = preload("res://src/visual_novel/tutorial/card_focus_blockout.tscn")

var blockout
var focused = false


func _on_top_card_pressed():
	# For testing purposes only, has to be pressed twice, works only once
	var card = hand.get_top_card()
	add_event(card_button.pressed, card, "Clique na carta para jogá-la")

	
func _ready():
	# For testing purposes only
	add_event(mixed_button.pressed, mixed_drawpile, "Clique na pilha mista para pegar uma carta")
	add_event(unwind_button.pressed, unwind_drawpile, "Clique na pilha de descontrair para pegar uma carta")
	

func add_event(start_signal, object, text):
	# When "start_signal" is emitted, "object" is put on focus mode, and will unfocus when pressed
	start_signal.connect(focus.bind(object, text))
	if (object is Card):
		object.played.connect(unfocus.bind(object))
	elif (object is Drawpile):
		object.card_drawn.connect(unfocus.bind(object))


func focus(node, text):
	focused = true
	get_tree().paused = true
	if (node is Card):
		# for a card, GameState need to be unpaused. Pause all but card:
		game_state.process_mode = Node.PROCESS_MODE_WHEN_PAUSED
		unwind_drawpile.process_mode = Node.PROCESS_MODE_DISABLED
		mixed_drawpile.process_mode = Node.PROCESS_MODE_DISABLED
		timeline.process_mode = Node.PROCESS_MODE_DISABLED
		for cards in hand.card_list.get_children():
			cards.process_mode = Node.PROCESS_MODE_DISABLED
		node.process_mode = Node.PROCESS_MODE_WHEN_PAUSED
	else:
		node.process_mode = Node.PROCESS_MODE_WHEN_PAUSED
	blockout = card_focus_blockout.instantiate()
	node.add_child(blockout)
	blockout.fade_in(text)


func unfocus(node):
	if (not focused):
		return
	if (node is Card):
		game_state.process_mode = Node.PROCESS_MODE_INHERIT
		unwind_drawpile.process_mode = Node.PROCESS_MODE_INHERIT
		mixed_drawpile.process_mode = Node.PROCESS_MODE_INHERIT
		timeline.process_mode = Node.PROCESS_MODE_INHERIT
		for cards in hand.card_list.get_children():
			cards.process_mode = Node.PROCESS_MODE_INHERIT
		node.process_mode = Node.PROCESS_MODE_INHERIT
	else:
		node.process_mode = Node.PROCESS_MODE_INHERIT
	blockout.fade_out()
	blockout.queue_free()
	get_tree().paused = false
	focused = false
